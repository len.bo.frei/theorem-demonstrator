################################################################
# Strings containing the acceptable symbols used in this 
#particular calculus
################################################################
ACCEPTED_SYMBOLS = "()s0->="
SEPARATOR_SYMBOLS = ">="
NEGATION_SYMBOLS = "-"


################################################################
# Cleaning and validation of the theorem string
################################################################
"""
	Function clean_input(_in)
	parameters: input string
	returns: string containing only the allowed characters inside the input string
"""
def clean_input(_in):
	if len(_in) == 0:
		return _in
	_out = ""
	for c in _in:
		if c in ACCEPTED_SYMBOLS:
			_out +=c
	return _in

"""
	Function is_string_balanced(_in)
	parameters: input string
	returns: true if the parenthesis are balanced, 
			and no closing parenthesis proceeds an opening one.
"""
def is_string_balanced(_in):
	if len(_in) == 0:
		return False
	counter = 0
	for c in _in:
		if c == '(':
			counter += 1
		elif c == ')':
			counter -= 1
		if counter < 0:
			return False
	return counter == 0
	
"""
	Function is_string_separation_valid(_in)
	parameters: input string
	returns: true if there is one and only one separation sign
"""
def is_string_separation_valid(_in):
	count = 0
	for c in _in:
		if c in SEPARATOR_SYMBOLS:
			count += 1
	return count == 1

"""
	Function is_string_negation_valid(_in)
	parameters: input string
	returns: true if there is at most one negation sign
"""
def is_string_negation_valid(_in):
	count = 0
	for c in _in:
		if c in NEGATION_SYMBOLS:
			count += 1
		if count > 1:
			return False
	return True
	
"""
	Function is_theorem_valid(_in)
	parameters: input string
	returns: true if the string is balanced, has a correct separation and a correct negation
"""
def is_theorem_valid(_in):
	return is_string_balanced(_in) and is_string_separation_valid(_in) and is_string_negation_valid(_in)


################################################################
# Simple string manipulation functions
################################################################
def find_separator_position(_in):
	position = 0
	for c in _in:
		position += 1
		if c in SEPARATOR_SYMBOLS:
			return position
	return -1
	
	
################################################################
# Calculus implementation: axioms, negation and derivation rules.
################################################################
"""
	Function get_negated_theorem(_in)
	parameters: input string
	returns: given a theorem, returns its negation
"""
def get_negated_theorem(_in):
	out = "-" + _in
	return delete_double_negation(out)


"""
	Function delete_double_negation(_in)
	parameters: input string
	returns: deletes a double negation if any.
"""	
def delete_double_negation(_in):
	if len(_in) >= 2:
		if _in[0] == '-' and _in[1] == '-':
			return _in[2:len(_in) - 1]
	return _in
	
"""
	Function write_axiom1()
	returns: string with the single axiom of the calculus
"""
def write_axiom1():
	return "(0=0)"

"""
	The following functions check if a given derivation rule can be applied 
	and apply it. In their description only the derivation rule is given:
	Z and T represent variables of the calculus.
"""

"""
	Rule 1:
	
	(Z = Z)
	--------
	(s(Z) = s(Z))
"""
def can_apply_rule1(_in):
	position = find_separator_position(_in)
	if _in[position] == '=':
		return True
	else:
		return False

def apply_rule1(_in):
	return _in

"""
	Rule 2:
	
	(Z = Z)
	--------
	(s(Z) > Z)
"""	
def can_apply_rule2(_in):
	return False
	
def apply_rule2(_in):
	return _in
	
	
"""
	Rule 3:
	
	(Z > T)
	--------
	(s(Z) > T)
"""	
def can_apply_rule3(_in):
	return False
	
def apply_rule3(_in):
	return _in
	
"""
	Rule 4:
	
	(Z > T)
	--------
	-(Z = T)
"""		
def can_apply_rule4(_in):
	return False
	
def apply_rule4(_in):
	return _in
	
"""
	Rule 5:
	
	(Z > T)
	--------
	-(T = Z)
"""
def can_apply_rule5(_in):
	return False
	
def apply_rule5(_in):
	return _in
	
"""
	Rule 5:
	
	(Z > T)
	--------
	-(T > Z)
"""
def can_apply_rule6(_in):
	return False
	
def apply_rule6(_in):
	return _in


def produce_new_theorems(_oldTheorems):
	new_theorems = []
	for t in _oldTheorems:
		if can_apply_rule1(t):
			new_theorems.append(apply_rule1(t))
	return new_theorems
	
################################################################
# Theorem demonstrator
################################################################

"""
	After cleaning up the given theorem from not acceptable symbols
	and checking that it is syntactically correct, the function 
	proceeds to generate theorems starting from the axiom and 
	applying interactively the derivation rules. The process stops
	when the theorem or its opposite is found, or when a maximum 
	number of generated theorems is reached. 
"""	

MAX_LENGTH = 1000
def demonstrate_theorem(_in):
	theorem = clean_input(_in)
	if not is_theorem_valid(theorem):
		return "syntactically invalid"
	negated_theorem = get_negated_theorem(_in)
	
	isFound = False
	isTrue = False
	generated_theorems = [write_axiom1()]
	while not isFound and len(generated_theorems) < MAX_LENGTH:
		generated_theorems = produce_new_theorems(generated_theorems)
		if theorem in generated_theorems:
			isFound = True
			isTrue = True
		elif negated_theorem in generated_theorems:
			isFound = True
			isTrue = False
		isFound = True
	if not isFound:
		return "not decidable in this implementation."
	else:
		return isTrue
	
def print_result(_in):
	print "The theorem " + _in + " is " + str(demonstrate_theorem(_in))
	
################################################################
# Scripted tests. Will be substituted by main, in a better
# implementation
################################################################

print_result("(s(s(0))>s(s(s(0))))")